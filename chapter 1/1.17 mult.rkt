#lang racket
(define (mult a b)
  (cond ((= b 0) 0)
        (else (+ a (mult a (- b 1))))))


(define (double x)
  (* 2 x))

(define (halve x)
  (/ x 2))

(define (even? n)
  (= 0 (remainder n 2)))

(define (mult-f a b)
  (cond ((= b 0) 0)
        ((even? b) (double (mult-f a (halve b))))
        (else (+ a (mult-f a (- b 1))))))