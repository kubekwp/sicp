#lang racket
(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a) (sum term (next a) next b))))

(define (pi-sum a b)
  (sum (lambda (x) (/ 1.0 (* x (+ x 2)))) a (lambda (x) (+ x 4)) b))

(define (integral f a b dx)
  (* (sum f (+ a (/ dx 2)) (lambda (x) (+ x dx)) b) 
     dx))

(define (plus4 x) (+ x 4))

(define plus-4 (lambda (x) (+ x 4)))

(define (square x) (* x x))

(define (f x y)
  (define (f-helper a b)
    (+ (* x (square a))
       (* y b)
       (* a b)))
  (f-helper (+ 1 (* x y)) (- 1 y)))
  
(define (ff x y)
  ((lambda (a b) (+ (* x (square a)) 
                    (* y b)
                    (* a b)))
   (+ 1 (* x y))
   (- 1 y)))

(define (fff x y)
  (let ((a (+ 1 (* x y))) 
        (b (- 1 y)))
    (+ (* x (square a)) 
       (* y b) 
       (* a b))))
