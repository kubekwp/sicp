#lang racket
(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq) (accumulate op init (cdr seq)))))

(define (flatmap proc seq)
  (accumulate append null (map proc seq)))

(define (permutations s)
  (cond ((null? s) (list null))
        (else (accumulate append null
                  (map (lambda (elem)
                         (map (lambda (perm) (cons elem perm))
                              (permutations (remove elem s))))
                       s)))))

(define (permutations2 s)
  (cond ((null? s) (list null))
        (else (flatmap (lambda (elem)
                         (map (lambda (perm) (cons elem perm))
                              (permutations (remove elem s)))) 
                       s))))