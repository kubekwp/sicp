#lang racket
(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (accumulate op init (cdr seq)))))

(define (horner-eval x coef-seq)
  (accumulate (lambda (this-coef higher-terms) (+ this-coef (* x higher-terms)))
              0
              coef-seq))

