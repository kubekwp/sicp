#lang racket
(define (attach-tag tag x)
  (cons tag x))

(define (type-tag datum) 
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum (TYPE-TAG): " datum)))
  
(define (contents datum) 
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum (CONTENTS): " datum)))

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (error "No method for these types" (list op type-tags))))))

(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))

; scheme-number
(define (install-scheme-number op)
  (define (tag x) 
    (attach-tag 'scheme-number x))

  (define add (lambda (x y) (tag (+ x y))))
  (define sub (lambda (x y) (tag (- x y))))
  (define mul (lambda (x y) (tag (* x y))))
  (define div (lambda (x y) (tag (/ x y))))
  
  (define (make x) (tag x))

  (cond ((eq? 'add op) add)
        ((eq? 'sub op) sub)
        ((eq? 'mul op) mul)
        ((eq? 'div op) div)
        ((eq? 'make op) make)))

(define (make-sn x) ((install-scheme-number 'make) x))

; rational
(define (numer x) (car x))
(define (denom x) (cdr x))

(define (install-rational op)
  (define (tag x) (attach-tag 'rational x))
  
  (define (make n d) (tag (cons n d)))
  (define add (lambda (x y) (tag (make-rat (+ (* (numer x) (denom y)) (* (numer y) (denom x)))
                                           (* (denom x) (denom y))))))
  (define sub (lambda (x y) (tag (make-rat (- (* (numer x) (denom y)) (* (numer y) (denom x)))
                                           (* (denom x) (denom y))))))
  (define mul (lambda (x y) (tag (make-rat (* (numer x) (numer y))
                                           (* (denom x) (denom y))))))
  (define div (lambda (x y) (tag (make-rat (* (numer x) (denom y))
                                           (* (denom x) (numer y))))))
  
  (cond ((eq? 'add op) add)
        ((eq? 'sub op) sub)
        ((eq? 'mul op) mul)
        ((eq? 'div op) div)
        ((eq? 'make op) make)))

(define (make-rat n d) ((install-rational 'make) n d))

; complex
(define (square x) (* x x))

(define (add-complex z1 z2)
  (make-from-real-imag (+ (real-part z1) (real-part z2))
                       (+ (imag-part z1) (imag-part z2))))

(define (sub-complex z1 z2)
  (make-from-real-imag (- (real-part z1) (real-part z2))
                       (- (imag-part z1) (imag-part z2))))

(define (mul-complex z1 z2)
  (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                       (+ (angle z1) (angle z2))))

(define (div-complex z1 z2)
  (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                       (- (angle z1) (angle z2))))

; complex rectangular
(define (tag-rect z) (attach-tag 'rectangular z))

(define (real-part-rectangular z) (car z))
(define (imag-part-rectangular z) (cdr z))

(define (magnitude-rectangular z) 
  (sqrt (+ (square (real-part-rectangular z)) (square (imag-part-rectangular z))))) 
(define (angle-rectangular z) 
  (atan (imag-part-rectangular z) (real-part-rectangular z)))

(define (make-from-real-imag-rectangular x y)
  (cons x y))

(define (make-from-mag-ang-rectangular r a)
  (cons (* r (cos a)) (* r (sin a))))

; complex polar
(define (tag-pol z) (attach-tag 'polar z))

(define (real-part-polar z) 
  (* (magnitude-polar z) (cos (angle-polar z)))) 
(define (imag-part-polar z) 
  (* (magnitude-polar z) (sin (angle-polar z))))

(define (magnitude-polar z) (car z))
(define (angle-polar z) (cdr z))

(define (make-from-real-imag-polar x y)
  (cons (sqrt (+ (square x) (square y))) 
        (atan y x)))

(define (make-from-mag-ang-polar r a)
  (cons r a))

; complex interface
(define (tag-com z) (attach-tag 'complex z))

(define (real-part z)
  ((get 'real-part (type-tag z)) (contents z)))

(define (imag-part z)
  ((get 'imag-part (type-tag z)) (contents z)))

(define (magnitude z)
  ((get 'magnitude (type-tag z)) (contents z)))

(define (angle z)
  ((get 'angle (type-tag z)) (contents z)))

(define (make-from-real-imag x y)
  ((get 'make-from-real-imag '(rectangular)) x y))

(define (make-from-mag-ang r a)
  ((get 'make-from-mag-ang '(polar)) r a))

(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag '(complex)) x y))

(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang '(complex)) r a))

(define add-complex-i (lambda (x y) (tag-com (add-complex x y))))
(define sub-complex-i (lambda (x y) (tag-com (sub-complex x y))))
(define mul-complex-i (lambda (x y) (tag-com (mul-complex x y))))
(define div-complex-i (lambda (x y) (tag-com (div-complex x y))))

; table
(define (get op type-tags)
  (cond ((and (eq? op 'add) (eq? (car type-tags) 'scheme-number) (eq? (cadr type-tags) 'scheme-number)) (install-scheme-number 'add))
        ((and (eq? op 'sub) (eq? (car type-tags) 'scheme-number) (eq? (cadr type-tags) 'scheme-number)) (install-scheme-number 'sub))
        ((and (eq? op 'mul) (eq? (car type-tags) 'scheme-number) (eq? (cadr type-tags) 'scheme-number)) (install-scheme-number 'mul))
        ((and (eq? op 'div) (eq? (car type-tags) 'scheme-number) (eq? (cadr type-tags) 'scheme-number)) (install-scheme-number 'div))
        
        ((and (eq? op 'add) (eq? (car type-tags) 'rational) (eq? (cadr type-tags) 'rational)) (install-rational 'add))
        ((and (eq? op 'sub) (eq? (car type-tags) 'rational) (eq? (cadr type-tags) 'rational)) (install-rational 'sub))
        ((and (eq? op 'mul) (eq? (car type-tags) 'rational) (eq? (cadr type-tags) 'rational)) (install-rational 'mul))
        ((and (eq? op 'div) (eq? (car type-tags) 'rational) (eq? (cadr type-tags) 'rational)) (install-rational 'div))
        
        ((and (eq? op 'make-from-real-imag) (eq? (car type-tags) 'rectangular)) (lambda (x y) (tag-rect (make-from-real-imag-rectangular x y))))
        ((and (eq? op 'make-from-mag-ang) (eq? (car type-tags) 'rectangular)) (lambda (x y) (tag-rect (make-from-mag-ang-rectangular x y))))
        ((and (eq? op 'real-part) (eq? type-tags 'rectangular)) real-part-rectangular)
        ((and (eq? op 'imag-part) (eq? type-tags 'rectangular)) imag-part-rectangular)
        ((and (eq? op 'magnitude) (eq? type-tags 'rectangular)) magnitude-rectangular)
        ((and (eq? op 'angle) (eq? type-tags 'rectangular)) angle-rectangular)

        ((and (eq? op 'make-from-real-imag) (eq? (car type-tags) 'polar)) (lambda (x y) (tag-pol (make-from-real-imag-polar x y))))
        ((and (eq? op 'make-from-mag-ang) (eq? (car type-tags) 'polar)) (lambda (x y) (tag-pol (make-from-mag-ang-polar x y))))
        ((and (eq? op 'real-part) (eq? type-tags 'polar)) real-part-polar)
        ((and (eq? op 'imag-part) (eq? type-tags 'polar)) imag-part-polar)
        ((and (eq? op 'magnitude) (eq? type-tags 'polar)) magnitude-polar)
        ((and (eq? op 'angle) (eq? type-tags 'polar)) angle-polar)

        ((and (eq? op 'make-from-real-imag) (eq? (car type-tags) 'complex)) (lambda (x y) (tag-com (make-from-real-imag x y))))
        ((and (eq? op 'make-from-mag-ang) (eq? (car type-tags) 'complex)) (lambda (x y) (tag-com (make-from-mag-ang x y))))
        ((and (eq? op 'real-part) (eq? type-tags 'complex)) real-part)
        ((and (eq? op 'imag-part) (eq? type-tags 'complex)) imag-part)
        ((and (eq? op 'magnitude) (eq? type-tags 'complex)) magnitude)
        ((and (eq? op 'angle) (eq? type-tags 'complex)) angle)
        ((and (eq? op 'add) (eq? (car type-tags) 'complex) (eq? (cadr type-tags) 'complex)) add-complex-i)
        ((and (eq? op 'sub) (eq? (car type-tags) 'complex) (eq? (cadr type-tags) 'complex)) sub-complex-i)
        ((and (eq? op 'mul) (eq? (car type-tags) 'complex) (eq? (cadr type-tags) 'complex)) mul-complex-i)
        ((and (eq? op 'div) (eq? (car type-tags) 'complex) (eq? (cadr type-tags) 'complex)) div-complex-i)))
