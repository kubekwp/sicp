#lang racket
(define (square n)
  (* n n))

(define (next-odd n)
  (if (= n 2) 3
      (+ n 2)))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (divides? a b)
  (= (remainder b a) 0))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next-odd test-divisor)))))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (enum-i x y)
  (if (> x y) 
      null
      (cons x (enum-i (+ x 1) y))))

(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq) (accumulate op init (cdr seq)))))

;(define (sub-pairs n)
;  (accumulate append null (map (lambda (i) 
;                                 (map (lambda (j) (cons i j))
;                                      (enum-i 1 (- i 1))))
;                               (enum-i 1 n))))

(define (flatmap proc seq)
  (accumulate append null (map proc seq)))

(define (sub-pairs n)
  (flatmap (lambda (i)
             (map (lambda (j) (cons i j))
                  (enum-i 1 (- i 1)))) 
           (enum-i 1 n)))

(define (prime-pair? p) 
  (prime? (+ (car p) (cdr p))))

(define (prime-pairs n)
  (filter prime-pair? (sub-pairs n)))

(define (make-pair-sum p) (list (car p) (cdr p) (+ (car p) (cdr p))))

(define (sum-prime-pairs n)
  (map make-pair-sum (prime-pairs n)))