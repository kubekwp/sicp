#lang racket
(require "common.rkt")

(define (add x y) (+ x y))

(define (make-sparse-list l)
    (attach-tag 'sparse l))
  
(define (make-dense-list l)
    (attach-tag 'dense l))

(define (first-term term-list) (car (contents term-list)))
(define (rest-terms term-list) (attach-tag (type-tag term-list) (cdr (contents term-list))))

(define (empty-termlist? term-list) (null? (contents term-list)))

(define (make-term order coeff) (list order coeff))
  (define (order term) (car term))
  (define (coeff term) (cadr term))

  (define (adjoin-term term term-list)
        (attach-tag (type-tag term-list) (cons term (contents term-list))))

; 0 1 2 0 0 3 4 -> (5 1) (4 2) (1 3) (0 4)
;(to-sparse (make-dense-list '(1 2 3 4)))
(define (to-sparse term-list)
    (make-sparse-list (map (lambda (icoeff) (make-term (car icoeff) (cadr icoeff))) (reverse-index (contents term-list)))))

(define (add-terms-dense L1 L2)
    (cond ((empty-termlist? L1) L2)
          ((empty-termlist? L2) L1)
          (else 
           (cond ((> (length L1) (length L2))
                  (adjoin-term (first-term L1) (add-terms-dense (rest-terms L1) L2)))
                 ((< (length L1) (length L2))
                  (adjoin-term (first-term L2) (add-terms-dense L1 (rest-terms L2))))
                 (else (adjoin-term (+ (first-term L1) (first-term L2))
                                    (add-terms-dense (rest-terms L1) (rest-terms L2))))))))

(define (add-terms-sparse L1 L2)
    (cond ((empty-termlist? L1) L2)
          ((empty-termlist? L2) L1)
          (else
           (let ((t1 (first-term L1)) (t2 (first-term L2)))
             (cond ((> (order t1) (order t2))
                    (adjoin-term t1 (add-terms-sparse (rest-terms L1) L2)))
                   ((< (order t1) (order t2))
                    (adjoin-term t2 (add-terms-sparse L1 (rest-terms L2))))
                   (else (adjoin-term (make-term (order t1)
                                                 (add (coeff t1) (coeff t2)))
                                      (add-terms-sparse (rest-terms L1) 
                                                 (rest-terms L2)))))))))

;(define ld1 (make-dense-list '(1 2 3)))
;(define ld2 (make-dense-list '(4 5 6)))
;(define ld3 (make-dense-list '(9 0 0 4 0 0 5 6)))
;(define ld4 (make-dense-list '(2 1)))

;(define ls1 (make-sparse-list (list (make-term 4 2) (make-term 2 7))))
;(define ls2 (make-sparse-list (list (make-term 3 5) (make-term 1 4))))
;(define ls3 (make-sparse-list (list (make-term 4 1) (make-term 3 5) (make-term 2 6) (make-term 1 4))))

