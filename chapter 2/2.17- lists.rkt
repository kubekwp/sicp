#lang racket
(define (leng items)
  (define (leng-iter a count)
    (if (null? a)
        count
        (leng-iter (cdr a) (+ 1 count))))
  (leng-iter items 0))

(define (append list1 list2)
  (if (null? list1)
      list2
      (cons (car list1) (append (cdr list1) list2))))

(define (last-pair items)
  (if (null? (cdr items))
      items
      (last-pair (cdr items))))

(define (reverse items)
  (if (null? (cdr items))
      items
  (append (reverse (cdr items)) (list (car items)))))